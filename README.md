# aws-terraform

## Requirement: 
[terraform](https://www.terraform.io)

## How to run:
```
$ terraform init
$ terraform plan
$ terraform apply
```
