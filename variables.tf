variable "vpc_name" {
  description = "Name of VPC"
  type        = string
  default     = "stocks"
}

variable "vpc_cidr" {
  description = "CIDR block for VPC"
  type        = string
  default     = "10.0.0.0/16"
}

variable "vpc_azs" {
  description = "Availability zones for VPC"
  type        = list(string)
  default     = ["us-west-2a"]
}

variable "vpc_private_subnet" {
  description = "Private subnets for VPC"
  type        = list(string)
  default     = ["10.0.1.0/24"]
}

variable "vpc_public_subnet" {
  description = "Public subnets for VPC"
  type        = list(string)
  default     = ["10.0.100.0/24"]
}

variable "vpc_enable_nat_gateway" {
  description = "Enable NAT gateway for VPC"
  type        = bool
  default     = true
}

variable "vpc_tags" {
  description = "Tags to apply to resources created by VPC module"
  type        = map(string)
  default = {
    name   = "stocks"
  }
}

variable "asg_name" {
  description = "Name of Autoscaling group"
  type        = string
  default     = "stocks-asg"
}

variable "asg_min_size" {
  description = "Minimum size Autoscaling"
  type        = number
  default     = 2
}

variable "asg_max_size" {
  description = "Maximum size Autoscaling"
  type        = number
  default     = 5
}

variable "asg_desired_capacity" {
  description = "Desired Capacity Autoscaling"
  type        = number
  default     = 2
}

variable "asg_wait_for_capacity_timeout" {
  type        = number
  default     = 0
}

variable "launch_template_name" {
  description = "Launch template name"
  type        = string
  default     = "stocks-launch-template"
}

variable "image_ids" {
  description = "Image ID type"
  type        = string
  default     = "ami-013a129d325529d4d"
}

variable "instance_types" {
  description = "Instance types"
  type        = string
  default     = "t3.medium"
}

variable "availability_zones" {
  description = "Zone availability instances"
  type        = string
  default     = "us-west-2a"
}

variable "asg_policy_name" {
  type        = string
  default     = "stocks-asg-policy"
}

variable "asg_policy_type" {
  type	      = string
  default     = "TargetTrackingScaling"
}

variable "asg_estimated_instance_warmup" {
  type        = number
  default     = 300
}

variable "asg_target_value" {
  type        = number
  default     = 45
}

variable "asg_metric_type" {
  type        = string
  default     = "ASGAverageCPUUtilization"
}
