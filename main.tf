terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
    }
  }
}

provider "aws" {
  region = "us-west-2"
}

resource "aws_eip" "nat_gw" {
  count = 1
  tags = {
    Name = "nat_gw_ip"
  }
  vpc = true
}

module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "3.10.0"

  name = var.vpc_name
  cidr = var.vpc_cidr

  azs            = var.vpc_azs
  private_subnets = var.vpc_private_subnet
  public_subnets  = var.vpc_public_subnet

  enable_nat_gateway = var.vpc_enable_nat_gateway
  single_nat_gateway = true
  reuse_nat_ips      = true
  external_nat_ip_ids = "${aws_eip.nat_gw.*.id}"
}

module "asg" {
  source  = "terraform-aws-modules/autoscaling/aws"
  version = "~>4.1"

  name = var.asg_name
  min_size = var.asg_min_size
  max_size = var.asg_max_size
  desired_capacity = var.asg_desired_capacity
  wait_for_capacity_timeout = var.asg_wait_for_capacity_timeout
  health_check_type         = "EC2"
  vpc_zone_identifier       = module.vpc.private_subnets

  initial_lifecycle_hooks = [
    {
      name                  = "StartupLifeCycleHook"
      default_result        = "CONTINUE"
      heartbeat_timeout     = 60
      lifecycle_transition  = "autoscaling:EC2_INSTANCE_LAUNCHING"
    },
    {
      name                  = "TerminationLifeCycleHook"
      default_result        = "CONTINUE"
      heartbeat_timeout     = 180
      lifecycle_transition  = "autoscaling:EC2_INSTANCE_TERMINATING"
    }
  ]

  instance_refresh = {
    strategy = "Rolling"
    preferences = {
      min_healthy_percentage = 50
    }
    triggers = ["tag"]
  }

  # Launch template
  lt_name                = var.launch_template_name
  description		 = "Launch template configuration"
  update_default_version = true

  use_lt    = true
  create_lt = true

  image_id          = var.image_ids
  instance_type     = var.instance_types
  ebs_optimized     = true
  enable_monitoring = true

  block_device_mappings = [
    {
      # Root volume
      device_name = "/dev/sda1"
      no_device   = 0
      ebs = {
        delete_on_termination = true
        encrypted             = true
        volume_size           = 10
        volume_type           = "gp2"
      }
    }
  ]

  network_interfaces = [
    {
      delete_on_termination = true
      description           = "eth0"
      device_index          = 0
      security_groups       = [module.vpc.default_security_group_id]
    }
  ]

  placement = {
    availability_zone = var.availability_zones
  }
}

resource "aws_autoscaling_policy" "asg_policy" {
  name                      = var.asg_policy_name
  policy_type		    = var.asg_policy_type
  estimated_instance_warmup = var.asg_estimated_instance_warmup
  autoscaling_group_name = module.asg.autoscaling_group_name
  target_tracking_configuration {
    predefined_metric_specification {
      predefined_metric_type = var.asg_metric_type
    }
    target_value = var.asg_target_value
  }
}
